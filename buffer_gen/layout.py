from typing import *

from bag.layout.routing import TrackID
from bag.layout.template import TemplateBase
from abs_templates_ec.analog_core import AnalogBase

from sal.routing_grid import RoutingGridHelper
from sal.via import ViaDirection

from inverter_en_gen.layout import inverter_en
from resistor_array_gen.layout import resistor_array
from .params import buffer_layout_params


class layout(AnalogBase):
    """A 2 to 1 Multiplexer including inverter and tgate sub-blocks.

    Parameters
    ----------
    temp_db : :class:`bag.layout.template.TemplateDB`
           the template database.
    lib_name : str
       the layout library name.
    params : dict[str, any]
       the parameter values.
    used_names : set[str]
       a set of already used cell names.
    **kwargs :
       dictionary of optional parameters.  See documentation of
       :class:`bag.layout.template.TemplateBase` for details.
    """
    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        super().__init__(temp_db, lib_name, params, used_names, **kwargs)
        self._sch_params = None
    
    @classmethod
    def get_params_info(cls) -> Dict[str, str]:
        """
        Returns a dictionary containing parameter descriptions.

        Override this method to return a dictionary from parameter names to descriptions.

        Returns
        -------
        param_info : dict[str, str]
           dictionary from parameter name to description.
        """
        return dict(
            params='buffer_layout_params parameters object',
        )

    def draw_layout(self):
        """Draw the layout."""

        # make copies of given dictionaries to avoid modifying external data.
        params: buffer_layout_params = self.params['params'].copy()
        inv1_params = params.inverter1_params.copy()
        inv2_params = params.inverter2_params.copy()
        res_array_params = params.resistor_array_params.copy()

        inv1_params.show_pins = False
        inv2_params.show_pins = False
        res_array_params.show_pins = False

        # create layout masters for subcells we will add later
        inv1_master = self.new_template(temp_cls=inverter_en, params=dict(params=inv1_params))
        inv2_master = self.new_template(temp_cls=inverter_en, params=dict(params=inv2_params))
        res_array_master = self.new_template(temp_cls=resistor_array, params=dict(params=res_array_params))

        # add subcell instances
        inv1_inst = self.add_instance(inv1_master, 'inv1', loc=(0, 0), unit_mode=True)
        x1 = inv1_inst.bound_box.right_unit
        inv2_inst = self.add_instance(inv2_master, 'inv2', loc=(x1, 0), unit_mode=True)
        x2 = inv2_inst.bound_box.right_unit
        res_array_inst = self.add_instance(res_array_master, 'Res', loc=(x2+192, 0), unit_mode=True)    # TO_DO

        # get input/output wires from Subcells
        inv1_I_warr = inv1_inst.get_all_port_pins('in')[0]
        inv1_O_warr = inv1_inst.get_all_port_pins('out')[0]
        inv1_EN_warr = inv1_inst.get_all_port_pins('EN')[0]
        inv1_ENB_warr = inv1_inst.get_all_port_pins('ENB')[0]

        inv2_I_warr = inv2_inst.get_all_port_pins('in')[0]
        inv2_I_warr1 = inv2_inst.get_all_port_pins('in')[1]
        inv2_O_warr = inv2_inst.get_all_port_pins('out')[0]
        inv2_EN_warr = inv2_inst.get_all_port_pins('EN')[0]
        inv2_ENB_warr = inv2_inst.get_all_port_pins('ENB')[0]

        Res_M_warr = res_array_inst.get_all_port_pins('M')[0]
        Res_P_warr = res_array_inst.get_all_port_pins('P')[0]

        # get VDD/VSS wires from Subcells
        Res_VDD_warr = res_array_inst.get_all_port_pins('VDD')[0]
        Res_VDD1_warr = res_array_inst.get_all_port_pins('VDD')[1]

        inv1_VDD_warr = inv1_inst.get_all_port_pins('VDD')[0]
        inv1_VSS_warr = inv1_inst.get_all_port_pins('VSS')[0]
        inv2_VDD_warr = inv2_inst.get_all_port_pins('VDD')[0]
        inv2_VSS_warr = inv2_inst.get_all_port_pins('VSS')[0]

        # Connect wires
        self.connect_wires([inv1_EN_warr, inv2_EN_warr])
        self.connect_wires([inv1_ENB_warr, inv2_ENB_warr])

        # get layer IDs
        hm_layer = 4
        vm_layer = hm_layer + 1
        top_layer = vm_layer + 1

        # get vertical VDD TrackIDs
        top_w = self.grid.get_track_width(top_layer, 1, unit_mode=True)

        # TODO: depending on the process node vm_w might need to be wider than 1 (which works for sky130 tech def)
        # vm_w = self.grid.get_min_track_width(vm_layer, top_w=top_w, unit_mode=True)     # Set the Track width
        vm_w = 1

        # Connect Resistors using M1 and M2

        inv1_VDD_u_tid = TrackID(vm_layer, self.grid.coord_to_nearest_track(vm_layer, inv1_VDD_warr.upper),
                                 width=3 * vm_w)
        Res_VDD_l_tid = TrackID(vm_layer, self.grid.coord_to_nearest_track(vm_layer, Res_VDD_warr.lower),
                                width=vm_w)
        Res_M_u_tid = TrackID(vm_layer, self.grid.coord_to_nearest_track(vm_layer, Res_M_warr.upper) + 4,
                              width=5 * vm_w)
        Res_M_l_tid = TrackID(vm_layer, self.grid.coord_to_nearest_track(vm_layer, Res_M_warr.lower) - 4,
                              width=5 * vm_w)

        grid_helper = RoutingGridHelper(template_base=self,
                                        unit_mode=True,
                                        layer_id=vm_layer,
                                        track_width=vm_w)

        # connect VDD of each block to vertical M5
        inv2_I_Res_P = self.connect_to_tracks(
            [
                inv2_I_warr,
                inv2_I_warr1,
                grid_helper.connect_level_up(
                   warr_id=grid_helper.connect_level_up(warr_id=Res_P_warr,
                                                        via_dir=ViaDirection.TOP,
                                                        step=0.0),
                   via_dir=ViaDirection.TOP,
                   step=0.0
                )
            ],
            Res_M_u_tid)
        inv2_O_Res_M = self.connect_to_tracks(
            [
                inv2_O_warr,
                grid_helper.connect_level_up(
                    warr_id=grid_helper.connect_level_up(warr_id=Res_M_warr,
                                                         via_dir=ViaDirection.TOP,
                                                         step=0.0),
                    via_dir=ViaDirection.TOP,
                    step=0.0
                )
            ],
            Res_M_l_tid)
        inv1_O_inv2_I = self.connect_to_tracks([inv1_O_warr, inv2_I_warr, inv2_I_warr1], inv1_VDD_u_tid)
        VDD = self.connect_to_tracks([ inv1_VDD_warr, inv2_VDD_warr], Res_VDD_l_tid)
        VDD = self.connect_to_tracks([ Res_VDD_warr, Res_VDD1_warr], Res_VDD_l_tid)
        VSS = self.connect_wires([inv1_VSS_warr, inv2_VSS_warr])

        # Set Bounding Box and size of top block
        tot_box = inv1_inst.bound_box.merge(inv2_inst.bound_box)
        tot_box = tot_box.merge(res_array_inst.bound_box)
        self.set_size_from_bound_box(vm_layer, tot_box, round_up=True)

        # add pins on wires
        self.add_pin('VDD', [Res_VDD_warr, inv1_VDD_warr, inv2_VDD_warr], show=params.show_pins)
        self.add_pin('VSS', [inv1_VSS_warr, inv2_VSS_warr], show=params.show_pins)

        # re-export pins on subcells.
        self.reexport(inv1_inst.get_port('in'), show=params.show_pins)
        self.reexport(inv2_inst.get_port('out'), show=params.show_pins)
        self.reexport(inv2_inst.get_port('EN'), show=params.show_pins)
        self.reexport(inv2_inst.get_port('ENB'), show=params.show_pins)

        # compute schematic parameters.
        self._sch_params = dict(
            inv1_params=inv1_master.sch_params,
            inv2_params=inv2_master.sch_params,
            res_array_params=res_array_master.sch_params,
        )

    @property
    def sch_params(self) -> Dict[str, Any]:
        return self._sch_params


class buffer(layout):
    """
    Class to be used as template in higher level layouts
    """
    def __init__(self, temp_db, lib_name, params, used_names, **kwargs):
        super().__init__(temp_db, lib_name, params, used_names, **kwargs)
