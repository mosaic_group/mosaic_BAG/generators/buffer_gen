v {xschem version=3.4.4 file_version=1.2
}
G {}
K {}
V {}
S {}
E {}
N 370 -110 400 -110 {
lab=out}
N 320 -150 320 -140 {
lab=VDD}
N 320 -80 320 -60 {
lab=VSS}
N 400 -110 420 -110 {
lab=out}
N 350 -110 370 -110 {
lab=out}
N 300 -150 300 -140 {
lab=EN}
N 300 -80 300 -60 {
lab=ENB}
N 140 -80 140 -60 {
lab=VSS}
N 120 -80 120 -60 {
lab=ENB}
N 50 -110 110 -110 {
lab=in}
N 140 -160 140 -140 {
lab=VDD}
N 120 -160 120 -140 {
lab=EN}
N 170 -110 290 -110 {
lab=in_b}
N 220 -260 220 -110 {
lab=in_b}
N 220 -260 260 -260 {
lab=in_b}
N 360 -260 400 -260 {
lab=out}
N 400 -260 400 -110 {
lab=out}
C {devices/iopin.sym} 80 -290 2 0 {name=p4 lab=in}
C {devices/iopin.sym} 80 -340 2 0 {name=p5 lab=VDD}
C {devices/iopin.sym} 80 -320 2 0 {name=p6 lab=VSS}
C {inverter_en_gen/inverter_en_templates_xschem/inverter_en/inverter_en.sym} 260 -70 0 0 {name=inv2
}
C {devices/lab_pin.sym} 420 -110 2 0 {name=l1 sig_type=std_logic lab=out}
C {devices/lab_pin.sym} 320 -150 1 0 {name=l3 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 320 -60 3 0 {name=l5 sig_type=std_logic lab=VSS}
C {resistor_array_gen/resistor_array_templates_xschem/resistor_array/resistor_array.sym} 250 -220 0 0 {name=Res
spiceprefix=X
}
C {devices/lab_pin.sym} 310 -310 1 0 {name=l2 sig_type=std_logic lab=VDD}
C {devices/iopin.sym} 80 -270 2 0 {name=p1 lab=out}
C {devices/iopin.sym} 80 -240 2 0 {name=p2 lab=EN}
C {devices/iopin.sym} 80 -220 2 0 {name=p3 lab=ENB}
C {devices/lab_pin.sym} 50 -110 0 0 {name=l4 sig_type=std_logic lab=in}
C {devices/lab_pin.sym} 300 -150 1 0 {name=l6 sig_type=std_logic lab=EN}
C {devices/lab_pin.sym} 300 -60 3 0 {name=l7 sig_type=std_logic lab=ENB}
C {inverter_en_gen/inverter_en_templates_xschem/inverter_en/inverter_en.sym} 80 -70 0 0 {name=inv1
}
C {devices/lab_pin.sym} 140 -60 3 0 {name=l9 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 120 -60 3 0 {name=l11 sig_type=std_logic lab=ENB}
C {devices/lab_pin.sym} 140 -160 1 0 {name=p7 sig_type=std_logic lab=VDD}
C {devices/lab_pin.sym} 120 -160 1 0 {name=p8 sig_type=std_logic lab=EN}
C {devices/lab_pin.sym} 220 -180 0 0 {name=l8 sig_type=std_logic lab=in_b}
