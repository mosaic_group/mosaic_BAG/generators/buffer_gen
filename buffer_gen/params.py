#! /usr/bin/env python3

from __future__ import annotations  # allow class type hints within same class
from dataclasses import dataclass
from typing import *

from sal.params_base import *

from inverter_en_gen.params import inverter_en_layout_params
from resistor_array_gen.params import resistor_array_layout_params


@dataclass
class buffer_layout_params(LayoutParamsBase):
    """
    Parameter class for buffer_gen

    Args:
    ----
    inverter1_params : inverter_en_layout_params
        Parameters for first inverter_en sub-generators

    inverter2_params : inverter_en_layout_params
        Parameters for second inverter_en sub-generators

    resistor_array_params : resistor_array_layout_params
        Parameters for resistor_array sub-generator

    show_pins : bool
        True to create pin labels
    """

    inverter1_params: inverter_en_layout_params
    inverter2_params: inverter_en_layout_params
    resistor_array_params: resistor_array_layout_params
    show_pins: bool

    @classmethod
    def finfet_defaults(cls, min_lch: float) -> buffer_layout_params:
        inv1_params = inverter_en_layout_params.finfet_defaults(min_lch)
        inv1_params.seg_dict = {'n': 4, 'p': 6}
        inv1_params.ndum = 6

        inv2_params = inverter_en_layout_params.finfet_defaults(min_lch)
        inv2_params.seg_dict = {'n': 60, 'p': 180}
        inv1_params.ndum = 2

        return buffer_layout_params(
            inverter1_params=inv1_params,
            inverter2_params=inv2_params,
            resistor_array_params=resistor_array_layout_params.finfet_defaults(min_lch),
            show_pins=True,
        )

    @classmethod
    def planar_defaults(cls, min_lch: float) -> buffer_layout_params:
        inv1_params = inverter_en_layout_params.planar_defaults(min_lch)
        inv1_params.seg_dict = {'n': 4, 'p': 6}
        inv1_params.ndum = 6

        inv2_params = inverter_en_layout_params.planar_defaults(min_lch)
        inv2_params.seg_dict = {'n': 60, 'p': 180}
        inv1_params.ndum = 2

        return buffer_layout_params(
            inverter1_params=inv1_params,
            inverter2_params=inv2_params,
            resistor_array_params=resistor_array_layout_params.planar_defaults(min_lch),
            show_pins=True,
        )


@dataclass
class buffer_params(GeneratorParamsBase):
    layout_parameters: buffer_layout_params
    measurement_parameters: List[MeasurementParamsBase]

    @classmethod
    def defaults(cls, min_lch: float) -> buffer_params:
        return buffer_params(
            layout_parameters=buffer_layout_params.defaults(min_lch=min_lch),
            measurement_parameters=[]
        )
